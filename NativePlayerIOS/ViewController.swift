////
////  ViewController.swift
////  NativePlayerIOS
////
////  Created by Adrian Suryo Abiyoga on 13/03/20.
////  Copyright © 2020 Adrian Suryo Abiyoga. All rights reserved.
////
//
//import UIKit
//import AVKit
//import AVFoundation
//import GoogleInteractiveMediaAds
//
//class ViewController: UIViewController, IMAAdsLoaderDelegate, IMAAdsManagerDelegate {
//
//    var player : AVPlayer!
//    var playerContainerView: UIView!
//     var adsLoader: IMAAdsLoader!
//    var contentPlayhead: IMAAVPlayerContentPlayhead?
//    var adsManager: IMAAdsManager!
//      var playerViewController = AVPlayerViewController()
//
//
//    func setUpContentPlayer() {
//
//      contentPlayhead = IMAAVPlayerContentPlayhead(avPlayer: player)
//      NotificationCenter.default.addObserver(
//          self,
//          selector: "contentDidFinishPlaying:",
//          name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
//          object: player.currentItem);
//    }
//    func setUpAdsLoader() {
//      adsLoader = IMAAdsLoader(settings: nil)
//      adsLoader.delegate = self
//    }
//
//    func contentDidFinishPlaying(notification: NSNotification) {
//      // Make sure we don't call contentComplete as a result of an ad completing.
//        if ((notification.object as! AVPlayerItem) == player.currentItem) {
//        // NOTE: This line will cause an error until the next step, "Request Ads".
//        adsLoader!.contentComplete()
//      }
//    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view.
////        setUpPlayerContainerView()
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//        createPlayer()
//    }
//
//
//
//
//    // Set  up constraints for the player container view.
//    private func setUpPlayerContainerView() {
//        playerContainerView = UIView()
//        playerContainerView.backgroundColor = .black
//        view.addSubview(playerContainerView)
//        playerContainerView.translatesAutoresizingMaskIntoConstraints = false
//        playerContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
//        playerContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
//        playerContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3).isActive = true
//
//        if #available(iOS 11.0, *) {
//            playerContainerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
//        } else {
//            playerContainerView.topAnchor.constraint(equalTo: topLayoutGuide.topAnchor).isActive = true
//        }
//    }
//    func requestAds() {
//      // Create ad display container for ad rendering.
//        let adDisplayContainer = IMAAdDisplayContainer(adContainer: playerViewController.view, companionSlots: nil)
//      // Create an ad request with our ad tag, display container, and optional user context.
//      let request = IMAAdsRequest(
//          adTagUrl: "https://static.rctiplus.id/vmap/vmap_live_0_55652_android_defaultlive.xml",
//          adDisplayContainer: adDisplayContainer,
//          contentPlayhead: contentPlayhead,
//          userContext: nil)
//
//      adsLoader.requestAds(with: request)
//    }
//    func createPlayer() {
////        let url = URL(string: "https://vod.rctiplus.id/vod/89166/1/3/854/manifest.m3u8")
////
////        let controller = AVPlayerViewController()
////        controller.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
////        var player: AVPlayer?
////
////        if let url = url {
////            player = AVPlayer(url: url)
////        }
////
////        if let player = player {
////            controller.player = player
////            controller.player?.rate = 1
////        }
////
////        view.addSubview(controller.view)
//
//        let videoURL = URL(string: "https://vod.rctiplus.id/vod/89166/1/3/854/manifest.m3u8")
//       player = AVPlayer(url: videoURL!)
//
//        playerViewController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
//        playerViewController.player = player
//        playerViewController.player?.rate = 1
//
////        view.addSubview(playerViewController.view)
//        self.present(playerViewController, animated: true) {
//            self.playerViewController.player!.play()
//        }
//        setUpAdsLoader()
//        requestAds()
//    }
//    // MARK: - IMAAdsLoaderDelegate
//
//      func adsLoader(_ loader: IMAAdsLoader!, adsLoadedWith adsLoadedData: IMAAdsLoadedData!) {
//        // Grab the instance of the IMAAdsManager and set ourselves as the delegate.
//        adsManager = adsLoadedData.adsManager
//        adsManager.delegate = self
//
//        // Create ads rendering settings and tell the SDK to use the in-app browser.
//        let adsRenderingSettings = IMAAdsRenderingSettings()
//        adsRenderingSettings.webOpenerPresentingController = self
//
//        // Initialize the ads manager.
//        adsManager.initialize(with: adsRenderingSettings)
//      }
//
//      func adsLoader(_ loader: IMAAdsLoader!, failedWith adErrorData: IMAAdLoadingErrorData!) {
//        print("Error loading ads: \(adErrorData.adError.message)")
//        player.play()
//      }
//
//      // MARK: - IMAAdsManagerDelegate
//
//      func adsManager(_ adsManager: IMAAdsManager!, didReceive event: IMAAdEvent!) {
//        if event.type == IMAAdEventType.LOADED {
//          // When the SDK notifies us that ads have been loaded, play them.
//          adsManager.start()
//        }
//      }
//
//      func adsManager(_ adsManager: IMAAdsManager!, didReceive error: IMAAdError!) {
//        // Something went wrong with the ads manager after ads were loaded. Log the error and play the
//        // content.
//        print("AdsManager error: \(error.message)")
//        player.play()
//      }
//
//      func adsManagerDidRequestContentPause(_ adsManager: IMAAdsManager!) {
//        // The SDK is going to play ads, so pause the content.
//        player.pause()
//      }
//
//      func adsManagerDidRequestContentResume(_ adsManager: IMAAdsManager!) {
//        // The SDK is done playing ads (at least for now), so resume the content.
//        player.play()
//      }
//    }
//
//
//

import AVFoundation
import GoogleInteractiveMediaAds
import UIKit

class ViewController: UIViewController, IMAAdsLoaderDelegate, IMAAdsManagerDelegate {

  static let kTestAppContentUrl_MP4 = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"

  @IBOutlet weak var playButton: UIButton!
  @IBOutlet weak var videoView: UIView!
  var contentPlayer: AVPlayer?
  var playerLayer: AVPlayerLayer?

  var contentPlayhead: IMAAVPlayerContentPlayhead?
  var adsLoader: IMAAdsLoader!
  var adsManager: IMAAdsManager!

  static let kTestAppAdTagUrl =
      "https://rc-static.rctiplus.id/vmap/vmap_ima_vod_484_android_defaultvod.xml"

  override func viewDidLoad() {
    super.viewDidLoad()

    playButton.layer.zPosition = CGFloat.greatestFiniteMagnitude

    setUpContentPlayer()
    setUpAdsLoader()
  }

  override func viewDidAppear(_ animated: Bool) {
    playerLayer?.frame = self.videoView.layer.bounds
  }

  @IBAction func onPlayButtonTouch(_ sender: AnyObject) {
    //contentPlayer.play()
    requestAds()
    playButton.isHidden = true
  }

  func setUpContentPlayer() {
    // Load AVPlayer with path to our content.
    guard let contentURL = URL(string: ViewController.kTestAppContentUrl_MP4) else {
      print("ERROR: please use a valid URL for the content URL")
      return
    }
    contentPlayer = AVPlayer(url: contentURL)

    // Create a player layer for the player.
    playerLayer = AVPlayerLayer(player: contentPlayer)

    // Size, position, and display the AVPlayer.
    playerLayer?.frame = videoView.layer.bounds
    videoView.layer.addSublayer(playerLayer!)

    // Set up our content playhead and contentComplete callback.
    contentPlayhead = IMAAVPlayerContentPlayhead(avPlayer: contentPlayer)
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(ViewController.contentDidFinishPlaying(_:)),
      name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
      object: contentPlayer?.currentItem);
  }

  @objc func contentDidFinishPlaying(_ notification: Notification) {
    // Make sure we don't call contentComplete as a result of an ad completing.
    if (notification.object as! AVPlayerItem) == contentPlayer?.currentItem {
      adsLoader.contentComplete()
    }
  }

  func setUpAdsLoader() {
    adsLoader = IMAAdsLoader(settings: nil)
    adsLoader.delegate = self
  }

  func requestAds() {
    // Create ad display container for ad rendering.
    let adDisplayContainer = IMAAdDisplayContainer(adContainer: videoView, companionSlots: nil)
    // Create an ad request with our ad tag, display container, and optional user context.
    let request = IMAAdsRequest(
        adTagUrl: ViewController.kTestAppAdTagUrl,
        adDisplayContainer: adDisplayContainer,
        contentPlayhead: contentPlayhead,
        userContext: nil)
    print(contentPlayer?.currentItem?.duration.seconds)
    request?.contentDuration = Float(contentPlayer?.currentItem?.duration.seconds ?? 0)

    adsLoader.requestAds(with: request)
  }

  // MARK: - IMAAdsLoaderDelegate

  func adsLoader(_ loader: IMAAdsLoader!, adsLoadedWith adsLoadedData: IMAAdsLoadedData!) {
    // Grab the instance of the IMAAdsManager and set ourselves as the delegate.
    adsManager = adsLoadedData.adsManager
    adsManager.delegate = self

    // Create ads rendering settings and tell the SDK to use the in-app browser.
    let adsRenderingSettings = IMAAdsRenderingSettings()
    adsRenderingSettings.webOpenerPresentingController = self

    // Initialize the ads manager.
    adsManager.initialize(with: adsRenderingSettings)
  }

  func adsLoader(_ loader: IMAAdsLoader!, failedWith adErrorData: IMAAdLoadingErrorData!) {
    print("Error loading ads: \(adErrorData.adError.message)")
    contentPlayer?.play()
  }

  // MARK: - IMAAdsManagerDelegate

  func adsManager(_ adsManager: IMAAdsManager!, didReceive event: IMAAdEvent!) {
    if event.type == IMAAdEventType.LOADED {
      // When the SDK notifies us that ads have been loaded, play them.
      adsManager.start()
    }
  }

  func adsManager(_ adsManager: IMAAdsManager!, didReceive error: IMAAdError!) {
    // Something went wrong with the ads manager after ads were loaded. Log the error and play the
    // content.
    print("AdsManager error: \(error.message)")
    contentPlayer?.play()
  }

  func adsManagerDidRequestContentPause(_ adsManager: IMAAdsManager!) {
    // The SDK is going to play ads, so pause the content.
    contentPlayer?.pause()
  }

  func adsManagerDidRequestContentResume(_ adsManager: IMAAdsManager!) {
    // The SDK is done playing ads (at least for now), so resume the content.
    contentPlayer?.play()
  }
}
